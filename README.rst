======
README
======

:Info: See <https://bitbucket.org/ImmortalPC/c-backtrace> for repo.
:Author: NUEL Guillaume (ImmortalPC)
:Date: $Date: 2013-11-07 $
:Revision: $Revision: 10 $
:Description: Can detect a SEGFAULT and view the call stack


**Description**
---------------
| Can detect a SEGFAULT and view the call stack.
| You have 2 choices:

- Use g++ 4.8 and the option adress-sanitize.
- Use the Backtrace module with a single include.


**Require**
-----------
g++ (4.8 for adress-sanitize), python (2 or 3)


**Code** **Format**
-------------------
- The code is in UTF-8
- The code is indented with real tabs (\\t)
- The code is intended to be displayed with a size of 4 to tab (\\t)
- The fine lines are type LF (\\n)


**Standards**
-------------
- Comments are in doxygen.


**Usage**
---------
With g++4.8 and adress-sanitize
::

	void foo()
	{
		int* p = 0;
		*p = 42;
		p++;
	}

	int main( int argc, char* argv[] )
	{
		//getBackTraceIfCrash();// <- A METTRE EN 1er !
		iniTBacktraceSystem();

		foo();

		return 0;
	}

Compilation:

>>> g++-4.8 -std=gnu++0x -ggdb3 -fsanitize=address -fno-omit-frame-pointer src/example.cpp -o bin/fsanitize_address && cd bin && ./fsanitize_address 2>&1 | ./asan_symbolize.py

| --------------------------------------------------------------------------

With g++ and the backtrace module

::

	#include "backtrace/backtrace.h"

	void foo()
	{
		int* p = 0;
		*p = 42;
		p++;
	}

	int main( int argc, char* argv[] )
	{
		//getBackTraceIfCrash();// <- A METTRE EN 1er !
		iniTBacktraceSystem();

		foo();

		return 0;
	}

Compilation:

>>> g++-4.8 -ggdb3 src/example.cpp -o bin/module && cd bin && ./module
>>> ./backtrace.py ./module





**IDE** **RST**
---------------
RST créé grace au visualisateur: http://rst.ninjs.org/
