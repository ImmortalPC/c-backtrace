#!/usr/bin/python
# coding= utf8
import os, sys, re;
import subprocess;

EXEC_NAME = 'a.out';

if os.name == 'nt':
	EXEC_NAME += '.exe';

################################################################################
# @brief Main
# @param[in] argv	{List<Str>} Les arguments de la ligne de commande
# @return {int} 0 Si tout s'est bien passé
##
def main( argv ):
	global EXEC_NAME;
	if len(argv) > 1:
		if os.path.isfile(argv[1]):
			EXEC_NAME = argv[1];
		elif os.path.isfile(argv[1]+'.exe'):
			EXEC_NAME = argv[1]+'.exe';

	if not os.path.isfile(EXEC_NAME):
		print EXEC_NAME+' not found';
		return 1;

	fileData = None;
	with open('backtrace.log','r') as fp:
		fileData = fp.read();

	addrs = None;
	if os.name == 'nt':
		addrs = re.findall(r'\n([A-Z0-9a-z]+)  ', fileData);
	else:
		addrs = re.findall(r' 0x([A-Z0-9a-z]+)', fileData);
	ret = None;
	for addr in addrs:
		if os.name == 'nt':
			ret = execute(['addr2line', '-fCe', EXEC_NAME, addr]);
		else:
			ret = execute(['addr2line', '-pfCe', EXEC_NAME, addr]);
		if ret[1] != 0:
			print ret;
		if os.name == 'nt':
			ret = ret[0].replace('\r','').split('\n')[1];
			fileData = fileData.replace(addr, '['+ret.strip(' \t\r\n')+']');
		else:
			fileData = fileData.replace('[0x'+addr+']', '['+ret[0].strip(' \t\r\n')+']');
	with open('backtrace_CONVERTED.log', 'w') as fp:
		fp.write(fileData);
	print fileData;
	return 0;


def execute( cmd ):
	p = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);
	retVal = p.wait();
	return (p.stdout.read(), retVal);


if __name__ == '__main__':
	exit(main(sys.argv));
