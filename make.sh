#!/bin/bash

# Avec Adress sanitizer
echo -e '\033[1;32mAddressSanitizer----------------------------------------------------------\033[0m';
g++-4.8 -std=gnu++0x -ggdb3 -fsanitize=address -fno-omit-frame-pointer src/example.cpp -o bin/fsanitize_address && cd bin && ./fsanitize_address 2>&1 | ./asan_symbolize.py

# Nétoyage
cd ..

echo
echo -e '\033[1;32mModule Backtrace----------------------------------------------------------\033[0m';
# Avec le module personnel.
g++-4.8 -ggdb3 src/example.cpp -o bin/module && cd bin && ./module
./backtrace.py ./module
rm *.log

# Nétoyage
cd ..

echo
echo -e '\033[1;32mAddressSanitizer----------------------------------------------------------\033[0m';
# Avec le module personnel.
cd src_example_qt
qmake && make && rm *.o Makefile
cd ../bin && ./example_qt 2>&1 | ./asan_symbolize.py

# Nétoyage
cd ..
