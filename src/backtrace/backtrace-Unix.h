#ifndef BACKTRACE_UNIX_h
#define BACKTRACE_UNIX_h
#include <stdio.h>
#include <stdlib.h>
#include "backtrace.h"

#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
	#define __USE_GNU
#endif
#include <execinfo.h>
#include <signal.h>
#include <string.h>// Pour strsignal
#include <ucontext.h>
#include <cxxabi.h>
// This structure mirrors the one found in /usr/include/asm/ucontext.h
typedef struct _sig_ucontext {
	unsigned long		uc_flags;
	struct ucontext*	uc_link;
	stack_t				uc_stack;
	struct sigcontext	uc_mcontext;
	sigset_t			uc_sigmask;
} sig_ucontext_t;


/***************************************************************************//*!
* @brief Backtrace version linux
* @warning NE PAS OUBLIER DE COMPILER AVEC CFLAGS += -rdynamic<br />
*/

void iniTBacktraceSystem();
void crit_err_hdlr( int si_signo, siginfo_t* info, void* ucontext );

#include "backtrace-Unix.inl"
#endif// BACKTRACE_UNIX_h
