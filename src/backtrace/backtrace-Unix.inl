#ifndef BACKTRACE_UNIX_h
#include "backtrace-Unix.h"
#endif

/***********************************************************************//*!
* @brief Installe le système de capture des signaux ( sigaction )
* @warning NE PAS OUBLIER DE COMPILER AVEC<br />
* CFLAGS += -Wl,--export-all-symbols (WINDOWS), -rdynamic (LINUX)
*/
void iniTBacktraceSystem()
{
	/*
	struct sigaction {
		void     (* sa_handler)   (int);// sa_handler indique l'action affectée au signal signum, et peut être SIG_DFL pour l'action par défaut, SIG_IGN pour ignorer le signal, ou un pointeur sur une fonction de gestion de signaux.
		void     (* sa_sigaction) (int, siginfo_t *, void *);
		sigset_t    sa_mask;			// sa_mask fournit un masque de signaux à bloquer pendant l'exécution du gestionnaire. De plus le signal ayant appelé le gestionnaire est bloqué à moins que les attributs SA_NODEFER ou SA_NOMASK soient précisés.
		int         sa_flags;			// sa_flags spécifie un ensemble d'attributs qui modifient le comportement du gestionnaire de signaux. Il est formé par un OU binaire ( | ) entre les options suivantes :
		void     (* sa_restorer)  (void);// Obsolète ( DO NOT USE ! )
	}
	*/
	struct sigaction sigact;
	sigact.sa_sigaction = crit_err_hdlr;
	sigact.sa_flags = SA_RESTART | SA_SIGINFO;

	if( sigaction(SIGSEGV, &sigact, (struct sigaction *)NULL) != 0 )
	{
		fprintf(stderr, "[file " __FILE__ ", line %d]: Error setting signal handler for %d (%s)\n", __LINE__, SIGSEGV, strsignal(SIGSEGV));
		exit(EXIT_FAILURE);
	}
	if( sigaction(SIGILL, &sigact, (struct sigaction *)NULL) != 0 )
	{
		fprintf(stderr, "[file " __FILE__ ", line %d]: Error setting signal handler for %d (%s)\n", __LINE__, SIGILL, strsignal(SIGILL));
		exit(EXIT_FAILURE);
	}
	if( sigaction(SIGFPE, &sigact, (struct sigaction *)NULL) != 0 )
	{
		fprintf(stderr, "[file " __FILE__ ", line %d]: Error setting signal handler for %d (%s)\n", __LINE__, SIGFPE, strsignal(SIGFPE));
		exit(EXIT_FAILURE);
	}
}


/***********************************************************************//*!
* @brief Génère un rapport d'erreur. Cette fonction est appelée via sigaction()
* en cas de crash.
* @param[in] si_signo		Numéro de signal
* @param[in,out] info		Pointeur sur les info du crash ( cf struct siginfo_t )
* @param[in,out] ucontext	Pointeur sur les info du crash ( cf struct sig_ucontext_t )
* @note VERSION spécial Linux
* @warning NE PAS OUBLIER DE COMPILER AVEC CFLAGS += -Wl,--export-all-symbols et -rdynamic<br />
* (-finstrument-functions n'est pas du tout utilisé ICI)
*/
void crit_err_hdlr( int si_signo, siginfo_t* info, void* ucontext )
{
	/*
	siginfo_t {
		int     si_signo;		// Numéro de signal
		int     si_errno;		// Numéro d'erreur
		int     si_code;		// Code du signal
		pid_t   si_pid;			// PID de l'émetteur
		uid_t   si_uid;			// UID réel de l'émetteur
		int     si_status;		// Valeur de sortie
		clock_t si_utime;		// Temps utilisateur écoulé
		clock_t si_stime;		// Temps système écoulé
		sigval_t si_value;		// Valeur de signal
		int     si_int;			// Signal Posix.1b
		void *  si_ptr;			// Signal Posix.1b
		void *  si_addr;		// Emplacement d'erreur
		int     si_band;		// Band event
		int     si_fd;			// Descripteur de fichier
	}
	*/

	/***********************************************************************
	* Ouverture du fichier
	*/
	FILE* fp_backtrace = fopen(CRASH_FILENAME, CRASH_FILE_OPTION);
	if( !fp_backtrace ){
		fp_backtrace = stderr;
		dg("Impossible d'ouvrir le fichier <" CRASH_FILENAME ">");
	}

	/***********************************************************************
	* On Affiche la pile
	*/
	sig_ucontext_t* uc = (sig_ucontext_t*)ucontext;

	void* caller_address = (void *)uc->uc_mcontext.eip; // x86 specific

	fprintf(fp_backtrace, "[SU] Got signal %d (%s), faulty address is %p, from %p\n", si_signo, strsignal(si_signo), info->si_addr, caller_address);
	fflush(fp_backtrace);

	void* trace[MAX_DEPHT];
	int size = backtrace(trace, MAX_DEPHT);

	// Overwrite sigaction with caller's address
	trace[1] = caller_address;

	char** messages = backtrace_symbols(trace, size);
	char* mangled_name = 0, *offset_begin = 0, *offset_end = 0;

	// i=1 => Skip first stack frame (points here)
	for( int i=size-1; i>1 && messages != NULL; --i )// Lecture de la pile de bas en haut
	{
		mangled_name = 0;
		offset_begin = 0;
		offset_end = 0;

		// Find parantheses and +address offset surrounding mangled name
		for( char* p = messages[i]; *p; p++ )
		{
			if (*p == '(')
				mangled_name = p;
			else if (*p == '+')
				offset_begin = p;
			else if (*p == ')')
			{
				offset_end = p;
				break;
			}
		}

		// If the line could be processed, attempt to demangle the symbol
		if( mangled_name && offset_begin && offset_end && mangled_name < offset_begin )
		{
			*mangled_name++ = '\0';
			*offset_begin++ = '\0';
			*offset_end++ = '\0';

			#ifdef _CXXABI_H// defined(_CXXABI_H)
				int status;
				char* real_name = abi::__cxa_demangle(mangled_name, 0, 0, &status);

				// if demangling is successful, output the demangled function name
				if (status == 0)
					fprintf(fp_backtrace, "[%d] %p <%s> : %s+%s%s\n", size-i-1, trace[i], messages[i], real_name, offset_begin, offset_end);
				// otherwise, output the mangled function name
				else
					fprintf(fp_backtrace, "[%d] %p <%s>: %s+%s%s\n", size-i-1, trace[i], messages[i], mangled_name, offset_begin, offset_end);

				free(real_name);
			#else// if defined(_CXXABI_H) else !defined(_CXXABI_H)
				fprintf(fp_backtrace, "[%d] %p <%s> : %s+%s%s\n", size-i-1, trace[i], messages[i], mangled_name, offset_begin, offset_end);
			#endif// !defined(_CXXABI_H)
		}
		// otherwise, print the whole line
		else
			fprintf(fp_backtrace, "[%d] %p <%s>\n", size-i-1, trace[i], messages[i]);
	}

	free(messages);

	fseek(fp_backtrace, -1, SEEK_CUR);
	fprintf(fp_backtrace, " <- Crash here !\n");

	if( fp_backtrace != stderr )
		fclose(fp_backtrace);
	exit(EXIT_FAILURE);
}
