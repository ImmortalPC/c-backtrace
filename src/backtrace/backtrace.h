#ifndef BACKTRACE_h
#define BACKTRACE_h

#include <stdio.h>
#include <stdlib.h>

#define CRASH_FILENAME "backtrace.log"
#define CRASH_FILE_OPTION "w"
#define MAX_DEPHT 500

#ifndef dg
// Système d'erreur SANS EXIT
#	define dg( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); fflush(stderr);}
// Système d'erreur AVEC EXIT
#	define dgE( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); exit(__LINE__);}
#endif

#if defined(_WIN32) || defined(WIN32) || defined(WIN64) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#	include "backtrace-Win32.h"
#elif defined (linux)
#	include "backtrace-Unix.h"
#else
#	warning "Systeme d'exploitation inconnu !"
#	define iniTBacktraceSystem()
#endif

#endif// BACKTRACE_h
